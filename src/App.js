import './App.css';
import React from 'react';
import Login from './components/Login.js';
import { useState, useEffect } from 'react';
import Home from './components/Home.js';
import Wishlist from './components/Wishlist.js';
import firebase from "./components/Firebase.js";
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import { setUid,fetchWishlist } from "./components/Firebase.js";



function App() {
    const [user, setUser] = useState(null);
    useEffect(() => {
        firebase.auth().onAuthStateChanged(user => {
            setUser(user);
            setUid(user.uid);
            fetchWishlist();
        })
    }, []);

  return (
      <div className="App">
          <BrowserRouter>
              <Routes>
                  <Route path="/" element={user ? <Home user={user} /> : <Login />}> </Route>
                  <Route path="/wishlist" element={user ? <Wishlist user={user} /> : <Login />}></Route>
             
              </Routes>
          </BrowserRouter>
          
    </div>
  );
}

export default App;
