import React from 'react';
import Header from './Header.js';
import ShowWishlist from './ShowWishlist.js';
import 'bootstrap/dist/css/bootstrap.min.css';

const Wishlist = ({user}) => {
    
    return (
        <div>
            <Header user={user} />
            <ShowWishlist user={user}/>
        </div>      
        );
}

export default Wishlist;