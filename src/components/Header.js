import React from 'react';
import { auth } from './Firebase.js';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import logo from '../img/favicon.ico';
import './Header.css';


function Header({ user }) {
   
    return (      
        <div className="header">
            <Navbar expand="lg" variant="light" bg="black">
                   
                <a href="/"><img src={logo}/></a>
               
                    <Col id="h">
                        <Button id="user" variant="light"><FontAwesomeIcon icon={faUser} />{user.displayName}</Button>
                    <a href="/wishlist"><Button id="heart" variant="light"><FontAwesomeIcon icon={faHeart} /></Button></a>
                        <Button variant="danger" onClick={() => auth.signOut()}>Logout</Button>
                    </Col>               
                </Navbar>        
       </div>
    )
}

export default Header;