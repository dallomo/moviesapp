import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import ShowHome from './ShowHome';

function Home({ user }) {  
    return (
        <div>
            <Header user={user} />
            <ShowHome />
        </div>
    );
}

export default Home;