import React from 'react';
import { useState, useEffect } from 'react';
import { removeFav, getWishMovie } from "./Firebase.js";
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeartBroken } from '@fortawesome/free-solid-svg-icons';

const ShowWishlist = ({ user }) => {
    const [wish, setWish] = useState([]);
    const [data, setData] = useState([]);

    useEffect(() => {
        const temp = setTimeout(() => {
            var m = getWishMovie('');
            setData(m);

        }, 1500)

        return () => clearTimeout(temp);
    }, []);

    useEffect(() => {
        const temp = setTimeout(() => {
            setWish(data);
        }, 1500)

    }, [data]);

    return (
        <div className="Wishlist">
            <Container>
                <br />
                <input id="search" type="text" placeholder="Search wishlist..." onChange={e => {
                    setWish(getWishMovie(e.target.value))
                }} /><br /><br />
                <Table striped hover bordered>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Year</th>
                            <th>Gender</th>
                            <th>Remove from wishlist</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            wish.map(x => (
                              <tr>
                                    <td>{x['title']}</td>
                                    <td>{x['year']}</td>
                                    <td>{x['genre']}</td>
                                    <td><Button variant="light" onClick={() => setWish(removeFav(x))}><FontAwesomeIcon icon={faHeartBroken} /></Button></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </Container>
        </div>
    );      
}

export default ShowWishlist;