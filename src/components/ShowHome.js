import React from 'react';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { useState, useEffect } from 'react';
import { fetchMovies, fetchCustomMovies,addFav } from './Firebase.js';
import './ShowHome.css';

function ShowHome() {
    const [movie, setMovie] = useState([]);
    const [data, setData] = useState([]);
   
    useEffect(() => {
        const temp = setTimeout(() => {
            var m = fetchMovies();
            setData(m);

        }, 1500)
 
        return () => clearTimeout(temp);
    }, []);

    useEffect(() => {
        const temp = setTimeout(() => {
            setMovie(data);
        }, 1500)

    }, [data]);
    
    return (
        <div>
            <Container>
                <br />
                <input id="search" type="text" placeholder="Search movie..." onChange={e => {
                    setMovie(fetchCustomMovies(e.target.value))
                }} /><br /><br />
                <Table striped hover bordered>

                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Year</th>
                            <th>Gender</th>
                            <th>Add to wishlist</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            movie.map(x => (

                                <tr>
                                    <td>{x['title']}</td>
                                    <td>{x['year']}</td>
                                    <td>{x['genre']}</td>
                                    <td><Button variant="light" onClick={(e) => addFav(e, x)}><FontAwesomeIcon icon={faHeart} /></Button></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </Container>
        </div>
    );
}

export default ShowHome;

        

