import firebase from "firebase/compat/app";
import 'firebase/compat/auth';
import 'firebase/compat/database';
import 'firebase/compat/firestore';

var userID;
var wl = [];
var r = [];

const firebaseConfig = {
    apiKey: "AIzaSyA4WArTUCWWb3FCdTgqV_WXnZntkqcWC2k",
    authDomain: "cloud-lab5.firebaseapp.com",
    databaseURL: "https://cloud-lab5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cloud-lab5",
    storageBucket: "cloud-lab5.appspot.com",
    messagingSenderId: "1078158184570",
    appId: "1:1078158184570:web:d6716af4b670d002576bc3"
};


firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });

const db = firebase.database();
const firestore = firebase.firestore();

export function fetchMovies() {
    var list = [];
    var movie = db.ref("/movies-list");
    movie.orderByKey().on('child_added', snapshot => {
        const state = snapshot.val();
        list.push(state);
        
    })
    return list;
}

export function fetchCustomMovies(input) {
    var list = [];
    var movie = db.ref("/movies-list");

    if (!isNaN(+input)) {
        movie.orderByChild('year').equalTo(parseInt(input)).on('child_added', snapshot => {
            const state = snapshot.val();
            list.push(state);

        })
        return list;
    } else if (input === "") {
        return fetchMovies();
    } else {
        movie.orderByChild('title').startAt(input).endAt(input+'utf8ff').on('child_added', snapshot => {
            const state = snapshot.val();
            console.log(state);
            list.push(state);
        })
 
        movie.orderByChild('genre').startAt(input).endAt(input).on('child_added', snapshot => {
            const state = snapshot.val();
            console.log(state);
            list.push(state);
        })
        return list;
    }
    
}

export function setUid(userid) {
    userID = userid;
    
}

const addWish = (m) => {
    firestore.collection("wishlist").doc(userID).update({
        id: firebase.firestore.FieldValue.arrayUnion(m)
    });
    console.log("data added");
}
const removeWish = (m) => {
    firestore.collection("wishlist").doc(userID).update({
        id: firebase.firestore.FieldValue.arrayRemove(m)
    });
    console.log("data removed");
}


export function addFav(e, movie) {
    e.preventDefault();
    wl.push(movie);
    addWish(movie);
        
}

export function removeFav(movie) {
    
    const arr = wl.filter((item) => item !== movie);
    console.log(arr);
    removeWish(movie);
    return getWishMovie('');
}


export const fetchWishlist = () => {
    wl = [];
    r = [];
    firestore.collection("wishlist").doc(userID).get().then((querySnapshot) => {
        if (querySnapshot.exists) {
            r = querySnapshot.data();
            for (var i = 0; i < r['id'].length; i++) {
                if (isDuplicate(wl, r['id'][i])) {
                    console.log("element already exist");
                } else {
                    wl.push(r['id'][i]);
                }
               
            }
            console.log(wl);
            
        } else {
            console.log("error");
        }
    });
    
    
    
        
}

const isDuplicate = (data, obj) =>
    data.some((el) =>
        Object.entries(obj).every(([key, value]) => value === el[key])
    );

export function getWishMovie(input) {
    var m = [];
    if (input == '') {
        console.log(wl);
        return wl;
    } else if (!isNaN(+input)) {
        var tmp = [];
        for (var i = 0; i < wl.length; i++) {
            if (wl[i]['year'] == parseInt(input)) {
                tmp.push(wl[i]);
            }
        }
        return tmp;
    } else {
        var tmp = [];
        for (var i = 0; i < wl.length; i++) {
            if (wl[i]['genre'] == input || wl[i]['title'].startsWith(input)) {
                tmp.push(wl[i]);
            }
        }
        return tmp;
    }
}

export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;
